#ifndef GESTORALUMNESH_H_INCLUDED
#define GESTORALUMNESH_H_INCLUDED


typedef int posicion;
struct listAlumnes *l;

typedef struct {
int dia;
int mes;
int any;
}sfecha;

typedef struct 
{	int num;
	char letra;
}sdni;

typedef struct{
char nombre [100];
sdni dni;
char correo[100];
int nota;
sfecha nacimiento;
bool sexo;
}alumne;

struct  listAlumnes{
	alumne a;
	struct listAlumnes *sig;
	//struct listAlumnes *previous;
};


void menu();
char  tecladoOpciones();
void agregarAlumne(alumne a);
bool leerarchivo(char txt[]);


struct listAlumnes *crearnodo (void);
struct listAlumnes *insertarnodo(struct listAlumnes *l,  alumne a,int (*f)( alumne a ,  alumne b));



#endif // GESTORALUMNESH_H_INCLUDED
