#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "gestoralumnesH.h"
static FILE * archivo;

void menu(){
	printf("----Gestor Alumnes----\n");
	printf("1)Afegir alumne\n2)Buscar DNI\n3)Buscar per nom\n4)Veure ultim alumne buscat\n5)Eliminar ultim alumne buscat\n0)Sortir\n");
}
char tecladoOpciones(){
	int ch;
	char opcion =-1;
	scanf(" %c",& opcion);
	while((ch =getchar()) !='\n');
	return opcion;
}
bool leerarchivo(char txt[]){
bool leido = false;
   archivo = fopen(txt, "r");
   if (archivo == NULL) {
      printf("Error... El fichero no se encuentra.\n");    
   } else { //printf("EXISTE ARCHIVO!\n");
      fgetc(archivo);
      if (feof(archivo)) {
         printf("Error...El fichero esta vacio.\n");
         
      } else {
         leido = true; //lee el contenido del archivo
         while(!feof(archivo)){
            alumne aux; 
            int bolsexo;
            if (fscanf(archivo,"%s%d%c%s%d%d/%d/%d %d\n",aux.nombre,&aux.dni.num,&aux.dni.letra,aux.correo,&aux.nota,&aux.nacimiento.dia,&aux.nacimiento.mes,&aux.nacimiento.any,&bolsexo))
            {
               aux.sexo=bolsexo;
               agregarAlumne(aux);
            }
         }
      }
   }
   fclose(archivo);
   return leido;
}

void agregarAlumne(alumne a){
   char c;
   if ((sizeof(a))==0) // si esta vacio
   {
     printf("Ingrese nombre completo:\n");
      gets(a.nombre);
   printf("Ingrese dni:\n");
     scanf("%d",&a.dni.num);
     c = getchar(); 
     a.dni.letra =c;
   printf("Ingrese correo:\n");
      gets(a.correo);
   printf("Ingrese nota:\n");
      scanf("%d",&a.nota);
   printf("Ingrese fecha de nacimiento:\n");
      scanf("%d %d %d",&a.nacimiento.dia,&a.nacimiento.mes,&a.nacimiento.any);
   printf("Ingrese sexo:\n");
   }
   printf("nom:%s\n",a.nombre );
   printf("nota:%d\n", a.nota);
   printf("dni:%d\n",a.dni.num );
   printf("lista --> %s %d%c %s %d %d/%d/%d %d\n",a.nombre,a.dni.num,a.dni.letra,a.correo,a.nota,a.nacimiento.dia,a.nacimiento.mes,a.nacimiento.any,a.sexo);
   //insertarnodo(*l,a);
   
}
struct listAlumnes *crearnodo (void){
   return (struct listAlumnes *)malloc(sizeof(struct  listAlumnes));
   }

struct listAlumnes *insertarnodo(struct listAlumnes *l,  alumne al, int (*f)( alumne a ,  alumne b)){
   struct listAlumnes *p,*q,*ant;
   int ind;
   q = crearnodo();
q -> a = al;
q -> sig =NULL;
if (l == NULL)
{
   l=q;
}
/*si es nula */
ant = NULL;
  p = l;
  ind = 0;
  while (p != NULL && ind == 0) {
    if ((*f)(al,p->a) <= 0) /* aqui hay que insertar */
      ind = 1;
    else {
      ant = p;
      p = p->sig;
    }
  }
  if (ant == NULL) { /* insercion al comienzo */
    q->sig = l;
    l = q;
  } else { /* insercion en medio o al final */
    ant->sig = q;
    q->sig = p;
  }
  return l;

   }